package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-ping/ping"
)

var pcif_debug string = "0"

type pcif struct {
	privKey []byte
	ssh     *ConnSSH
	c       chan int
}

func (pf *pcif) Init() {
	i := 0
	pf.privKey = make([]byte, len(_PRIVKEY))
	for i < len(_PRIVKEY) {
		pf.privKey[i] = _PRIVKEY[i] ^ _RANDOM0[i]
		i++
	}
	pf.ssh = &ConnSSH{}
	pf.c = make(chan int)
}

func (pf *pcif) WaitIp() error {
	go func() {
		p, err := ping.NewPinger(USBARMORYIP)
		if err != nil {
			pf.c <- -100
			return
		}

		p.Count = 1
		p.Run()
		stat := p.Statistics()
		if stat.PacketsSent != stat.PacketsRecv {
			pf.c <- -99
			return
		}
		pf.c <- 0
	}()

	select {
	case ret_code := <-pf.c:
		if ret_code != 0 {
			return fmt.Errorf("create connect fail")
		}
	case <-time.After(2 * time.Second):
		return fmt.Errorf("connect timeout")
	}

	return nil
}

func (pf *pcif) Connect() error {
	if pf.privKey == nil || pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	err := pf.WaitIp()

	err = pf.ssh.Connect(USBARMORY, pf.privKey)
	if err != nil {
		return err
	}

	err = pf.ssh.Start()
	if err != nil {
		return nil
	}

	util_check := fmt.Sprintf("%s", CMD_CHK_COUNT)
	res, err := pf.ssh.Command(util_check)
	if res[0:2] != "OK" && res[3:] != SHA_SIGNED {
		return fmt.Errorf("util count has some problem")
	}

	util_check = fmt.Sprintf("%s", CMD_CHK_ID)
	res, err = pf.ssh.Command(util_check)
	if res[0:2] != "OK" && res[3:] != SHA_SIGNED {
		return fmt.Errorf("util verify_id has some problem")
	}

	set_time := fmt.Sprintf("%s %s", CMD_SET_DATE, time.Now().Format(time.RFC3339))
	pf.ssh.Command(set_time)
	/*
		pf.Disconnect()
		err = pf.ssh.Connect(USBARMORY, pf.privKey)
		if err != nil {
			return err
		}

		err = pf.ssh.Start()
		if err != nil {
			return nil
		}
	*/
	return nil
}

func (pf *pcif) Disconnect() {
	if pf.ssh == nil {
		return
	}

	pf.ssh.Close()
	pf.ssh = &ConnSSH{}
}

func (pf *pcif) Close() {
	if pf.ssh == nil {
		return
	}

	pf.ssh.Close()
	pf.ssh = &ConnSSH{}
	pf.privKey = nil
}

func (pf *pcif) GetCount() (uint32, error) {
	if pf.ssh == nil {
		return 0, fmt.Errorf("pcif is not inited")
	}

	cmd := CMD_CURRENT_CNT
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return 0, err
	}

	cnt, err := strconv.ParseUint(res, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint32(cnt), nil
}

func (pf *pcif) GetMax() (uint32, error) {
	if pf.ssh == nil {
		return 0, fmt.Errorf("pcif is not inited")
	}

	cmd := CMD_GET_MAX
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return 0, err
	}

	cnt, err := strconv.ParseUint(res, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint32(cnt), nil
}

func (pf *pcif) VerifyId(key string) error {
	if pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	cmd := fmt.Sprintf("%s %s", CMD_VERIFY_ID, key)
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return err
	}

	if res[0:2] != "OK" {
		return fmt.Errorf(res)
	}

	return nil
}

func (pf *pcif) procKeyfile() error {
	if pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	/*
		if _, err := os.Stat(AUTHFILE); errors.Is(err, os.ErrNotExist) {
			return fmt.Errorf("keyfile is not in the same directory")
		}

		keyfile, err := os.Open(AUTHFILE)
		if err != nil {
			return fmt.Errorf("open keyfile failed")
		}
		defer keyfile.Close()
		keyc, err := ioutil.ReadAll(keyfile)
	*/

	rfilename := fmt.Sprintf("/tmp/%s", AUTHFILE)
	pf.ssh.UploadFile(AUTHFILE, rfilename)

	cmd := fmt.Sprintf("cd /tmp")
	_, err := pf.ssh.Command(cmd, 1)
	if err != nil {
		return err
	}

	/*
		cmd = fmt.Sprintf("echo %s > ~/%s", keyc, AUTHFILE)
		_, err = pf.ssh.Command(cmd)
		if err != nil {
			return err
		}
	*/

	cmd = fmt.Sprintf("gpg --decrypt -a %s 2>&1 | sed 1,2d > /tmp/key_config", AUTHFILE)
	_, err = pf.ssh.Command(cmd, 1)
	if err != nil {
		return err
	}

	cmd = fmt.Sprintf("cd /root")
	_, err = pf.ssh.Command(cmd, 1)
	if err != nil {
		return err
	}

	return nil
}

func (pf *pcif) KeyfileId() error {
	if pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	if err := pf.procKeyfile(); err != nil {
		return err
	}

	cmd := fmt.Sprintf("%s", CMD_VERIFY_ID)
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return err
	}

	if res[0:2] != "OK" {
		return fmt.Errorf(res)
	}
	return nil
}

func (pf *pcif) KeyfileSetMax() error {
	if pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	if err := pf.procKeyfile(); err != nil {
		return err
	}

	cmd := fmt.Sprintf("%s", CMD_SET_MAX)
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return err
	}

	if res[0:2] != "OK" {
		return fmt.Errorf(res)
	}

	return nil
}

func (pf *pcif) SetMax(max uint32) error {
	if pf.ssh == nil {
		return fmt.Errorf("pcif is not inited")
	}

	cmd := fmt.Sprintf("%s %d", CMD_SET_MAX, max)
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return err
	}

	if res[0:2] != "OK" {
		return fmt.Errorf(res)
	}

	return nil
}

func (pf *pcif) IncCount(arg string) (string, error) {
	if pf.ssh == nil {
		return "", fmt.Errorf("pcif is not inited")
	}

	cmd := fmt.Sprintf("%s %s", CMD_INC_CNT, arg)
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return "", err
	}

	if res[0:2] != "OK" {
		return "", fmt.Errorf(res)
	}

	return res[3:], nil
}

func (pf *pcif) ReadAuthFile() (config map[string]string, err error) {
	finfo, err := os.Stat(AUTHFILE)
	if err != nil {
		err = fmt.Errorf("no auth file exists")
		config = nil
		return
	}

	authfile, err := os.Open(AUTHFILE)
	if err != nil {
		config = nil
		return
	}
	defer authfile.Close()

	iv := _RANDOM0[100:116]
	dekey := make([]byte, 32)
	i := 0
	for i < 32 {
		dekey[i] = _RANDOM3[i+200] ^ _RANDOM4[i+200]
		i++
	}

	c, err := aes.NewCipher(dekey)
	if err != nil {
		config = nil
		return
	}

	cfb := cipher.NewCFBDecrypter(c, iv)
	leng := finfo.Size()
	data := make([]byte, leng)
	authfile.Read(data)
	de_data := make([]byte, leng)
	cfb.XORKeyStream(de_data, data)
	sz := string(de_data[:])

	config = make(map[string]string)
	scanner := bufio.NewScanner(strings.NewReader(sz))
	for scanner.Scan() {
		l := scanner.Text()

		if equ := strings.Index(l, "="); equ >= 0 {
			if k := strings.TrimSpace(l[:equ]); len(k) > 0 {
				v := ""
				if len(l) > equ {
					v = strings.TrimSpace(l[equ+1:])
				}
				config[k] = v
			}
		}
	}

	err = nil
	return
}

func (pf *pcif) GetExitCode() (int, error) {
	if pf.ssh == nil {
		return -1000, fmt.Errorf("pcif is not inited")
	}

	cmd := fmt.Sprintf("echo $?")
	res, err := pf.ssh.Command(cmd)
	if err != nil {
		return -1000, err
	}

	exit_code, _ := strconv.ParseInt(res[0:], 10, 32)
	if exit_code > 127 {
		exit_code = exit_code - 256
	}

	return int(exit_code), nil
}
