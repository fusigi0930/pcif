package main

const (
	AUTHFILE        string = "auth_file.bin"
	USBARMORYIP     string = "10.0.0.1"
	USBARMORY       string = "10.0.0.1:22"
	AKEY_KEY        string = "AKEY"
	DUMMY_001       string = "BKEY_CKEY_"
	AKEY_VERIFYID   string = "VERIFYID"
	AKEY_SETMAX     string = "SETMAX"
	CMD_CURRENT_CNT string = "count"
	CMD_INC_CNT     string = "count inc"
	CMD_GET_MAX     string = "count max"
	CMD_SET_MAX     string = "count setmax"
	CMD_VERIFY_ID   string = "verify_id"
	CMD_CHK_COUNT   string = "count check"
	CMD_CHK_ID      string = "verify_id check check"
	CMD_SET_DATE    string = "date -s"
	SHA_SIGNED      string = "8861311137217dbc096c972ef1ee5ac1fe4fc36a7437acb5c7bb15c656f1a9cf"

	BEGIN int64  = 10485760
	LOG_B int64  = 15728640
	BLOCK int64  = 512
	MAGIC uint64 = 0x35c3b9be4b02e877
)
