//go:build admin
// +build admin

package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"os"
	"strconv"
)

func usage() {
	usage := "=============================================\n" +
		"pcif-admin <options> [<pamameter>...]\n\n" +
		"options:\n" +
		"\tcount: get current counter\n" +
		"\tinc: increase current counter\n" +
		"\tmax: get current max\n" +
		"\tsetmax: set current max\n" +
		"\t\twith parameter max value\n" +
		"\tid: verify id\n" +
		"\t\twith parameter id string\n\n" +
		"\tksetmax: set max by read keyfile\n" +
		"\tkid: verify id by read keyfile\n" +
		"\tdump: dump sdcard log to file \n"

	fmt.Printf(usage)
}

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		usage()
		return
	}

	if args[0] == "dump" {
		if len(args) != 3 {
			fmt.Printf("need sdcard node and output file name\n")
			return
		}

		dumpSdLog(args[:])
		return
	}

	pcif := &pcif{}
	pcif.Init()
	defer pcif.Close()

	err := pcif.Connect()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if args[0] == "count" {
		cnt, err := pcif.GetCount()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Printf("counter: %d\n", cnt)
	} else if args[0] == "max" {
		max, err := pcif.GetMax()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Printf("max: %d\n", max)
	} else if args[0] == "inc" {
		bcode, err := pcif.IncCount(args[1])
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println("OK", bcode)
	} else if args[0] == "setmax" {
		if len(args) < 2 {
			fmt.Printf("are you kidding, no max value\n\n")
			usage()
			return
		}

		max, _ := strconv.ParseUint(args[1], 10, 32)
		err := pcif.SetMax(uint32(max))
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println("OK")
	} else if args[0] == "id" {
		if len(args) < 2 {
			fmt.Printf("are you kidding, no id key\n\n")
			usage()
			return
		}

		err := pcif.VerifyId(args[1])
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		fmt.Println("OK")
	} else if args[0] == "kid" {
		if err := pcif.KeyfileId(); err != nil {
			fmt.Printf("%s\n", err.Error())
			return
		}

		fmt.Println("OK")
	} else if args[0] == "ksetmax" {
		if err := pcif.KeyfileSetMax(); err != nil {
			fmt.Printf("%s\n", err.Error())
			return
		}

		fmt.Println("OK")
	} else {
		fmt.Printf("are you kidding, not support command\n\n")
		usage()
		return
	}
}

func dumpSdLog(arg []string) {
	fmt.Printf("%v\n", arg)
	f, err := os.OpenFile(arg[1], os.O_RDWR, 0644)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}
	defer f.Close()

	outfile, err := os.Create(arg[2])
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		return
	}
	defer outfile.Close()

	f.Seek(BEGIN, 0)
	info := make([]uint64, 2)

	binary.Read(f, binary.LittleEndian, &info)

	if info[0] != MAGIC {
		fmt.Printf("magic number is not correct\n")
		return
	}

	fmt.Printf("max: %d\n", info[1])
	for i := 0; i < int(info[1]); i++ {
		f.Seek(LOG_B+int64(i)*BLOCK, 0)
		buf := make([]byte, BLOCK)
		f.Read(buf)
		buf = bytes.Trim(buf, "\x00")
		sz := string(buf)
		outfile.Write([]byte(sz))
	}
}
