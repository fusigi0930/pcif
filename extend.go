//go:build extend
// +build extend

package main

import (
	"fmt"
)

func main() {
	pcif := &pcif{}
	pcif.Init()
	defer pcif.Close()

	err := pcif.Connect()
	if err != nil {
		fmt.Println("Connect failed, ERR: 0x200e1100")
		return
	}

	if err = pcif.KeyfileId(); err != nil {
		fmt.Printf("dongle ID incorrect ERR: 0x20063322\n")
		return
	}

	err = pcif.KeyfileSetMax()
	if err != nil {
		fmt.Println("Set failed, ERR: 0x200a0011")
		return
	}

	fmt.Println("OK")
}
