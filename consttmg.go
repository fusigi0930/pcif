package main

const (
	AUTHFILE        string = "auth_file.bin"
	USBARMORYIP     string = "10.0.0.10"
	USBARMORY       string = "10.0.0.10:22"
	AKEY_KEY        string = "AKEY"
	DUMMY_001       string = "BKEY_CKEY_"
	AKEY_VERIFYID   string = "VERIFYID"
	AKEY_SETMAX     string = "SETMAX"
	CMD_CURRENT_CNT string = "count"
	CMD_INC_CNT     string = "count-inc"
	CMD_GET_MAX     string = "max"
	CMD_SET_MAX     string = "setmax"
	CMD_VERIFY_ID   string = "id"
	CMD_CHK_COUNT   string = "count-check"
	CMD_CHK_ID      string = "id-check"
	CMD_SET_DATE    string = "date"
	SHA_SIGNED      string = "8861311137217dbc096c972ef1ee5ac1fe4fc36a7437acb5c7bb15c656f1a9cf"

	BEGIN int64  = 10485760
	LOG_B int64  = 15728640
	BLOCK int64  = 512
	MAGIC uint64 = 0x35c3b9be4b02e877
)
