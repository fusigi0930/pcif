//go:build admin
// +build admin

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

var conn = &ConnSSH{}

// argument 1 ssh-key path

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usasge: %s <sshkey-path>", os.Args[0])
	}

	priKey, err := ioutil.ReadFile(os.Args[1])
	if err != nil || priKey == nil {
		log.Fatalf("read private key file failed %v", err)
	}

	conn := ConnSSH{}
	conn.Connect("10.0.0.1:22", priKey)

	err = conn.Start()
	if err != nil {
		log.Fatalf("start shell failed")
	}

	set_time := fmt.Sprintf("date -s %s", time.Now().Format(time.RFC3339))
	conn.Command(set_time)
	conn.Close()

	conn.Connect("10.0.0.1:22", priKey)
	defer conn.Close()

	err = conn.Start()
	if err != nil {
		log.Fatalf("start shell failed")
	}

	res, _ := conn.Command("date")
	log.Println(res)

	res, _ = conn.Command("count")
	log.Println(res)

	conn.Command("exit")
}
