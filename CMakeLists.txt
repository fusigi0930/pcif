cmake_minimum_required(VERSION 3.15)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} $ENV{BUILD_ENV}/cmake)
set(CMAKE_SUPPRESS_REGENERATION TRUE)

project(pcif Go)

if ("$ENV{DEBUG}" STREQUAL "" OR NOT "$ENV{DEBUG}" STREQUAL "1")
    set(DFLAG "-w" "-s")
else()
    set(DFLAG ${DFLAG} -X 'main.ssh_debug=1')
endif()

include(go)

set(
    COMMON_SRCS
    ${CMAKE_CURRENT_LIST_DIR}/ssh.go
    ${CMAKE_CURRENT_LIST_DIR}/link-${CMAKE_SYSTEM_NAME}.go
    ${CMAKE_CURRENT_LIST_DIR}/pcif.go
    ${CMAKE_CURRENT_LIST_DIR}/autogen-constbin.go
)

if (NOT "$ENV{ARMORY_OS}" STREQUAL "TAMAGO")
    set(COMMON_SRCS ${COMMON_SRCS} ${CMAKE_CURRENT_LIST_DIR}/constdeb.go)
else()
    set(COMMON_SRCS ${COMMON_SRCS} ${CMAKE_CURRENT_LIST_DIR}/consttmg.go)
endif()

set(
    ADMIN_SRCS
    ${COMMON_SRCS}
    ${CMAKE_CURRENT_LIST_DIR}/admin.go
)

set(
    EXPORT_SRCS
    ${COMMON_SRCS}
    ${CMAKE_CURRENT_LIST_DIR}/export.go
)

set(
    EXTEND_SRCS
    ${COMMON_SRCS}
    ${CMAKE_CURRENT_LIST_DIR}/extend.go
)

set(
    GENBIN_SRCS
    ${CMAKE_CURRENT_LIST_DIR}/gen-binary.go
)

if (NOT "$ENV{SETMAX_VALUE}" STREQUAL "")
    set(EXTEND_VAL ${DFLAG} -X 'main.extend_setmax_val=$ENV{SETMAX_VALUE}')
else()
    set(EXTEND_VAL ${DFLAG})
endif()

add_go_exe(pcif-admin ADMIN_SRCS DFLAG)
add_go_exe(extend EXTEND_SRCS DFLAG)
add_go_dll(pcif EXPORT_SRCS DFLAG)

add_go_exe(genbin GENBIN_SRCS EXTEND_VAL)
add_go_prebuild(TARGET pcif-admin CMD autogenbin ACTION ${CMAKE_CURRENT_BINARY_DIR}/genbin ${CMAKE_CURRENT_LIST_DIR})

add_dependencies(autogenbin genbin)
add_dependencies(extend autogenbin)
add_dependencies(pcif autogenbin)
