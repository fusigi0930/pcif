//go:build dll
// +build dll

package main

/*
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
*/
import "C"
import (
	"encoding/base64"
	"unsafe"
)

var pif = &pcif{}

const (
	ERR_NOERROR        int = 0
	ERR_LINKLOST       int = 1
	ERR_CONNFAIL       int = 2
	ERR_CNTFULL        int = 3
	ERR_BUF_NOT_ENOUGH int = 4
	ERR_UNKNOW         int = 98
	ERR_INTERNAL_ERR   int = 99
)

//export Init
func Init() C.int {
	pif.Init()
	if err := pif.Connect(); err != nil {
		return C.int(ERR_CONNFAIL)
	}

	return C.int(ERR_NOERROR)
}

//export DeInit
func DeInit() {
	pif.Close()
}

//export GetBurnCode
func GetBurnCode(in *C.char, leng C.int, out *C.char, limit C.int, out_size *C.uint32_t, current_count *C.uint32_t, max *C.uint32_t) C.int {
	//buf := C.GoBytes(unsafe.Pointer(in), leng)
	//arg := base64.StdEncoding.EncodeToString(buf)
	arg := C.GoString(in)
	bcode, err := pif.IncCount(arg)
	if err != nil {
		if err.Error()[0:8] == "ERR_0001" {
			return C.int(ERR_CNTFULL)
		} else if err.Error()[0:8] == "ERR_0006" {
			e_code, _ := pif.GetExitCode()
			if e_code != 0 {
				return C.int(e_code)
			}
		} else {
			return C.int(ERR_UNKNOW)
		}
	}

	d64_bcode, err := base64.StdEncoding.DecodeString(bcode)
	if err != nil {
		return C.int(ERR_INTERNAL_ERR)
	}

	if int(limit) < len(d64_bcode) {
		return C.int(ERR_BUF_NOT_ENOUGH)
	}

	cs := (*C.char)(unsafe.Pointer(&d64_bcode[0]))

	C.memcpy(unsafe.Pointer(out), unsafe.Pointer(cs), C.size_t(len(d64_bcode)))

	if out_size != nil {
		*out_size = C.uint32_t(len(d64_bcode))
	}

	if current_count != nil {
		*current_count = C.uint32_t(uint(GetCurrentCount()))
	}

	if max != nil {
		*max = C.uint32_t(uint(GetMax()))
	}

	return C.int(ERR_NOERROR)
}

//export GetCurrentCount
func GetCurrentCount() C.int {
	cnt, _ := pif.GetCount()
	return C.int(cnt)
}

//export GetMax
func GetMax() C.int {
	max, _ := pif.GetMax()
	return C.int(max)
}

func main() {

}
