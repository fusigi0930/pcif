//go:build !windows
// +build !windows

package main

import (
	"fmt"

	"github.com/vishvananda/netlink"
)

func SetIp(szAddr string) error {
	usb0, err := netlink.LinkByName("usb0")
	if err != nil {
		return fmt.Errorf("usb0 is not exist")
	}

	addr, err := netlink.ParseAddr(szAddr)
	if err != nil {
		return fmt.Errorf("the addr: %s parse error", szAddr)
	}

	exists_addrs, _ := netlink.AddrList(usb0, netlink.FAMILY_V4)
	for _, eaddr := range exists_addrs {
		if addr.Equal(eaddr) {
			return nil
		}
	}

	netlink.AddrAdd(usb0, addr)
	return nil
}
