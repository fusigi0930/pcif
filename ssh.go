package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

type ConnSSH struct {
	c       *ssh.Client
	s       *ssh.Session
	out     io.Reader
	in      io.WriteCloser
	isStart bool
}

var ssh_debug string = "0"

func (conn *ConnSSH) Connect(host string, priKey []byte) error {
	if host == "" || priKey == nil {
		return fmt.Errorf("no host or key")
	}

	signer, err := ssh.ParsePrivateKey(priKey)
	if err != nil {
		return err
	}

	config := &ssh.ClientConfig{
		User: "root",
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	conn.c, err = ssh.Dial("tcp", host, config)
	if err != nil {
		conn.c = nil
		return err
	}

	conn.s, err = conn.c.NewSession()
	if err != nil {
		conn.c.Close()
		conn.c = nil
		conn.s = nil
		return err
	}

	sout, _ := conn.s.StdoutPipe()
	serr, _ := conn.s.StderrPipe()
	conn.out = io.MultiReader(sout, serr)
	conn.in, err = conn.s.StdinPipe()
	return nil
}

func (conn *ConnSSH) Close() {
	if conn.out != nil {
		conn.out = nil
	}
	if conn.in != nil {
		conn.in.Close()
		conn.in = nil
	}
	if conn.s != nil {
		conn.s.Close()
		conn.s = nil
	}
	if conn.c != nil {
		conn.c.Close()
		conn.c = nil
	}
	conn.isStart = false
}

func (conn *ConnSSH) Start() error {
	if conn.isStart == true {
		return nil
	}

	err := conn.s.Shell()
	if err != nil {
		log.Fatalf("failed to run shell, %s", err.Error())
	}

	conn.isStart = true
	time.Sleep(100 * time.Millisecond)
	conn.Read()
	return nil
}

func (conn *ConnSSH) Command(cmd string, ignoreRead ...int) (string, error) {
	if conn.s == nil || conn.c == nil || conn.out == nil || conn.in == nil {
		return "", fmt.Errorf("no handler")
	}

	c := make(chan int)

	if ssh_debug == "1" {
		fmt.Printf("---> ssh.command: %s\n", cmd)
	}

	go func() {
		conn.in.Write([]byte(cmd + "\n"))
		time.Sleep(500 * time.Millisecond)
		c <- 0
	}()
	<-c

	if ssh_debug == "1" {
		fmt.Printf("<--- ssh.command: %s\n", cmd)
	}

	var buf []byte
	var err error
	if len(ignoreRead) == 0 {
		buf, err = conn.Read()
		if err != nil {
			return "", err
		}
	} else {
		buf = []byte{'O', 'K', '\n'}
	}
	//ret := string(buf[len(cmd)+0:])
	ret := string(buf)
	res := strings.Split(ret, "\n")

	return res[0], nil
}

func (conn *ConnSSH) Read() ([]byte, error) {
	if conn.out == nil {
		return nil, fmt.Errorf("no stdout")
	}

	c := make(chan int)
	var ret []byte
	buf := make([]byte, 1024)
	timer := time.NewTimer(time.Millisecond * time.Duration(500))

	var err error
	var n int
	go func() {
		for err == nil {
			n, err = conn.out.Read(buf)
			ret = append(ret, buf...)
			if n != 1024 {
				break
			}
		}
		c <- 0
	}()
	select {
	case <-c:
		break
	case <-timer.C:
		err = fmt.Errorf("read timeout")
		break
	}

	return ret, err
}

func (conn *ConnSSH) UploadFile(filename string, dst string) error {
	if conn.c == nil {
		return fmt.Errorf("no ssh client")
	}

	sftp, err := sftp.NewClient(conn.c)
	if err != nil {
		return err
	}
	defer sftp.Close()

	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("file is not exist")
	}

	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("open file failed")
	}
	defer f.Close()

	dstf, err := sftp.Create(dst)
	if err != nil {
		return err
	}
	defer dstf.Close()

	if _, err = dstf.ReadFrom(f); err != nil {
		return err
	}

	return nil
}
