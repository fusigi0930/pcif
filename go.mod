module pcif

go 1.17

require (
	github.com/vishvananda/netlink v1.1.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)

require (
	github.com/ProtonMail/go-crypto v0.0.0-20220113124808-70ae35bab23f // indirect
	github.com/ProtonMail/go-mime v0.0.0-20190923161245-9b5a4261663a // indirect
	github.com/ProtonMail/gopenpgp v1.0.0 // indirect
	github.com/ProtonMail/gopenpgp/v2 v2.4.2 // indirect
	github.com/go-ping/ping v0.0.0-20211130115550-779d1e919534 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pkg/sftp v1.13.4 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/vishvananda/netns v0.0.0-20191106174202-0a2b9b5464df // indirect
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	golang.org/x/text v0.3.6 // indirect
)
