package main

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/ProtonMail/gopenpgp/v2/helper"
)

const (
	AUTOGEN_DIR      string = "autogen"
	AUTOGEN_FILE     string = "constbin.go"
	AUTOGEN_PATH     string = AUTOGEN_DIR + "-" + AUTOGEN_FILE
	AUTOGEN_AUTHFILE string = "auth_file.bin"

	AKEY_VERIFYID string = "AKEY_VERIFYID"
	AKEY_SETMAX   string = "AKEY_SETMAX"
)

var extend_setmax_val string = "0"

func get_param() (auth_config map[string]string) {
	auth_config = make(map[string]string)
	var ok bool
	auth_config[AKEY_VERIFYID], ok = os.LookupEnv(AKEY_VERIFYID)
	if !ok {
		panic(fmt.Errorf("\n==========================\nplease set ENV variable %s\n==========================\n", AKEY_VERIFYID))
	}

	auth_config[AKEY_SETMAX], ok = os.LookupEnv(AKEY_SETMAX)
	if !ok {
		panic(fmt.Errorf("\n==========================\nplease set ENV variable %s\n==========================\n", AKEY_SETMAX))
	}
	// dummy
	rand_max := 1 + rand.Intn(9)
	dummy_list := []string{"0M%d", "1M%d", "2M%d", "AM%d", "BM%d", "CM%d", "DM%d"}
	i := 0
	for i < rand_max {
		for _, dummy := range dummy_list {
			sz := fmt.Sprintf(dummy, i)
			auth_config[sz] = randString(2)
		}

		i++
	}
	return
}

func en_aes(iv []byte, key []byte, target string) (crypted []byte, err error) {
	if iv == nil || key == nil || target == "" {
		err = fmt.Errorf("encrypt information lost")
		crypted = nil
		return
	}

	c, err := aes.NewCipher(key)
	if err != nil {
		crypted = nil
		return
	}

	cfb := cipher.NewCFBEncrypter(c, iv)
	crypted = make([]byte, len(target))
	cfb.XORKeyStream(crypted, []byte(target))
	err = nil

	return
}

func gpg_enc(gpub_path string, txt string) {
	gpub_file, err := os.Open(gpub_path)
	if err != nil {
		panic(err)
	}
	defer gpub_file.Close()
	gpub, err := ioutil.ReadAll(gpub_file)

	if _, err := os.Stat(AUTOGEN_AUTHFILE); err == nil {
		os.Remove(AUTOGEN_AUTHFILE)
	}
	enfile, err := os.Create(AUTOGEN_AUTHFILE)
	if err != nil {
		fmt.Printf("Error: %s\n", err.Error())
		return
	}
	defer enfile.Close()

	armor, err := helper.EncryptMessageArmored(string(gpub[:]), txt)
	enfile.Write([]byte(armor))

}

func main() {
	privkey_path := os.Getenv("SSH_PRIVATE_KEY")
	priKey, err := ioutil.ReadFile(privkey_path)
	if err != nil {
		panic(err)
	}

	gpub_path := os.Getenv("GPG_PUB")

	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Printf("please input current folder")
		return
	}
	autogen_path := args[0] + "/" + AUTOGEN_PATH

	if _, err := os.Stat(autogen_path); err == nil {
		os.Remove(autogen_path)
	}

	genfile, err := os.Create(autogen_path)
	if err != nil {
		panic(err)
	}

	defer genfile.Close()

	genfile.Write([]byte("package main\n\n"))

	rand.Seed(time.Now().UnixNano())
	rand_max := 5 + rand.Intn(5)
	var rand0 []byte
	var rand1 []byte
	var rand2 []byte
	var rand3 []byte
	var rand4 []byte
	i := 0
	for i < rand_max {
		var sz string
		if i == 0 {
			rand0 = randBytes(4096)
			sz = fmt.Sprintf("% #x", rand0)
		} else if i == 1 {
			rand1 = randBytes(4096)
			sz = fmt.Sprintf("% #x", rand1)
		} else if i == 2 {
			rand2 = randBytes(4096)
			sz = fmt.Sprintf("% #x", rand2)
		} else if i == 3 {
			rand3 = randBytes(4096)
			sz = fmt.Sprintf("% #x", rand3)
		} else if i == 4 {
			rand4 = randBytes(4096)
			sz = fmt.Sprintf("% #x", rand4)
		} else {
			sz = fmt.Sprintf("% #x", randBytes(4096))
		}

		sz = strings.ReplaceAll(sz, " ", ", ")
		label := fmt.Sprintf("var _RANDOM%d = [...]byte { %s }\n\n", i, sz)
		genfile.Write([]byte(label))
		i++
	}

	// real
	i = 0
	for i < len(priKey) {
		priKey[i] = priKey[i] ^ rand0[i]
		i++
	}

	sz := fmt.Sprintf("% #x", priKey)
	sz = strings.ReplaceAll(sz, " ", ", ")
	priv_str := fmt.Sprintf("var _PRIVKEY = [...]byte { %s }\n\n", sz)
	genfile.Write([]byte(priv_str))

	setmax_val := make([]byte, 8)
	smv, _ := strconv.ParseUint(extend_setmax_val, 10, 32)
	binary.LittleEndian.PutUint64(setmax_val, uint64(smv))
	i = 0
	for i < len(setmax_val) {
		setmax_val[i] = setmax_val[i] ^ rand1[i] ^ rand2[i]
		i++
	}
	sz_setmax := fmt.Sprintf("% #x", setmax_val)
	sz_setmax = strings.ReplaceAll(sz_setmax, " ", ", ")
	setmax_str := fmt.Sprintf("var _EXTEND_SETMAX_VAL = [...]byte { %s }\n\n", sz_setmax)
	genfile.Write([]byte(setmax_str))

	config := get_param()
	var sz_auth_config string
	for k, v := range config {
		sz_auth_config = sz_auth_config + fmt.Sprintf("%s=%s\n", k, v)
	}

	//
	//debugfile, _ := os.Create("bbb.bin")
	//defer debugfile.Close()
	//debugfile.Write([]byte(sz_auth_config))
	//
	gpg_enc(gpub_path, sz_auth_config)

	// dummy
	rand_max = 5 + rand.Intn(5)
	i = 0
	for i < rand_max {
		sz := fmt.Sprintf("% #x", randBytes(4096))
		sz = strings.ReplaceAll(sz, " ", ", ")
		label := fmt.Sprintf("var TILL_RANDOM%d = [...]byte { %s }\n\n", i, sz)
		genfile.Write([]byte(label))
		i++
	}

	genfile.Write([]byte("\n\n"))
}

func randBytes(size int) (buf []byte) {
	if size <= 0 {
		size = 1
	}

	buf = make([]byte, size)
	rand.Read(buf)
	return
}

func randString(leng int) string {
	if leng <= 0 {
		leng = 1
	}

	buf := make([]byte, leng)
	i := 0
	for i < leng {
		buf[i] = 0x41 + byte(rand.Intn(26))
		i++
	}

	return string(buf)
}
